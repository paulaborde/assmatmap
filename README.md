# AssMatMap : Assistante Maternelle Map

Récupère la liste des assistantes maternelles de la plateforme https://www.assistantes-maternelles.net/ pour générer une carte.

# Fonctionnement

* Analyse chaque pages pour récupérer les liens par assistante maternelle
* Analyse chaque page assistante pour récupérer adresse
* Géocode adresse pour générer couple lat/long


# Utiliser le script

* Renommer le fichier configuration_example.json en configuration.json
* Renseigner dans le fichier configuration.json les credentials Here pour le geocoding
* Renseigner la liste des adresses à analyser


# Evolutions potentielles

* Input seule de la page mère avec gestion paging