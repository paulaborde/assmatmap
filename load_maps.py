# coding: utf8
"""
    Extract map of Ass Mat
    v1.0 02/02/2019
    paulaborde.g@gmail.com
"""

from bs4 import BeautifulSoup
import requests
import pandas as pd
import json

# Configuration
credential = json.load('configuration.json')
appid = credential['here']['appid']
appcode = credential['here']['appcode']

def geocode(address):
    searchtext = address.replace(' ', '+')
    url = "https://geocoder.api.here.com/6.2/geocode.json?app_id=%s&app_code=%s&searchtext=%s" % (appid, appcode, searchtext)
    try:
        r = requests.get(url)
        res = r.json()
        tmp_lat = res['Response']['View'][0]['Result'][0]['Location']['DisplayPosition']['Latitude']
        tmp_lng = res['Response']['View'][0]['Result'][0]['Location']['DisplayPosition']['Longitude']

    except:
        tmp_lat = 0
        tmp_lng = 0

    return({'lat':tmp_lat, 'lng':tmp_lng})

def pull_address(ass_url):
    print('Address parsing : %s' % ass_url)
    map_r = requests.get(ass_url)
    ass_html = map_r.text
    map_soup = BeautifulSoup(ass_html)

    for l in map_soup.find_all('p'):
        if l.get('style') == 'font-size:110%;text-align:center':
            tmp_address = l.get_text().replace('\n','')
            result = geocode(tmp_address)
    return({'ref':ass_url, 'lat':result['lat'], 'lng':result['lng']})


def pull_links(page_url):
    r = requests.get(page_url)
    html = r.text
    all_add = []
    soup = BeautifulSoup(html)

    for link in soup.find_all('a'):
        tmp_href = link.get('href')
        if 'assistante-maternelle-' in tmp_href and tmp_href not in all_add and 'forum' not in tmp_href:
            all_add.append(pull_address(tmp_href))

    return(all_add)

# Main
print('Start extract')
full_add = []
urls = ['https://www.assistantes-maternelles.net/toulouse-31500',
        'https://www.assistantes-maternelles.net/toulouse-31500?page=2',
        'https://www.assistantes-maternelles.net/toulouse-31500?page=3',
        'https://www.assistantes-maternelles.net/toulouse-31500?page=4',
        'https://www.assistantes-maternelles.net/toulouse-31500?page=5',
        'https://www.assistantes-maternelles.net/toulouse-31500?page=6']

for url_base in urls:
    print('Analyse : %s' % url_base)
    full_add += pull_links(url_base)

print('Export data')
df = pd.DataFrame(full_add)
df.to_csv('ass_matt_map.csv', sep=';', encoding='utf-8')
print(df)